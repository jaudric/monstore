package com.store.controllers;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.hateoas.mvc.ControllerLinkBuilder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import com.store.entities.Company;
import com.store.repositories.CompanyRepository;

@RestController
@RequestMapping("/secured/service")
public class ServiceController {
	
	
	 @Autowired
	 private CompanyRepository companyService;
	 
	    @RequestMapping(value ="test", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(value = HttpStatus.OK)
	    public @ResponseBody
	    //@PreAuthorize("hasAuthority('COMPANY_READ') and hasAuthority('DEPARTMENT_READ')")
	    String getAllUser() {
	        return "Test reuissi !!!";
	    }
	 
	 
	 @RequestMapping(method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(value = HttpStatus.OK)
	    public @ResponseBody
	    List<Company> getAll() {
	        return companyService.findAll();
	    }

	    @RequestMapping(value = "company/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(value = HttpStatus.OK)
	    public @ResponseBody
	    Company get(@PathVariable Long id) {
	        return companyService.findById(id).get();
	    }

	    @RequestMapping(value = "company/filter", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(value = HttpStatus.OK)
	    public @ResponseBody
	    Company get(@RequestParam String name) {
	        return companyService.findByName(name);
	    }

	    @RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(value = HttpStatus.OK)
	    public ResponseEntity<?> create(@RequestBody Company company) {
	        companyService.save(company);
	        HttpHeaders headers = new HttpHeaders();
	        ControllerLinkBuilder linkBuilder = linkTo(methodOn(ServiceController.class).get(company.getId()));
	        headers.setLocation(linkBuilder.toUri());
	        return new ResponseEntity<>(headers, HttpStatus.CREATED);
	    }

	    @RequestMapping(method = RequestMethod.PUT, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(value = HttpStatus.OK)
	    public void update(@RequestBody Company company) {
	        companyService.save(company); 
	    }

	    @RequestMapping(value = "company/{id}", method = RequestMethod.DELETE, produces = MediaType.APPLICATION_JSON_VALUE)
	    @ResponseStatus(value = HttpStatus.OK)
	    public void delete(@PathVariable Long id) {
	        companyService.deleteById(id);
	    }
	 
	 
}

package com.store.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.store.entities.Company;

public interface CompanyRepository extends JpaRepository<Company, Long>{
	
    //Company find(Long id);
	Optional<Company> findById(Long id);
    Company findByName(String name);

    /*List<Company> findAll();

    void create(Company company);

    Company update(Company company);

    void delete(Long id);*/

   // void delete(Company company);
}
